# Convert images to sprites
Put all the images you wish to use in your game in a folder, and name the images what you want the sprites to be named in code.

Run the following (where `PATH_TO_FOLDER` is the path of the folder that contains all the sprites for your game)
```bash
cargo run --release -- PATH_TO_FOLDER
```
Once the program is done, a `sprites.rs` file will appear containing all the sprites in the folder. This file can be moved to the `firmware/src` folder to have access to the sprites.
