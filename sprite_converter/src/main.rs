use std::{fs::File, io::Write, path::PathBuf, result::Result};

use image::{io::Reader, GenericImageView};
use walkdir::WalkDir;

fn main() -> Result<(), image::ImageError> {
    let images = match get_image_paths() {
        Ok(images) => images,
        Err(msg) => {
            println!("{msg}");
            return Ok(());
        }
    };

    let mut palette: Vec<(u8, u8, u8)> = Vec::new();
    palette.push((0, 0, 0)); // Index 0 of palette is always Transparent
    palette.push((0, 0, 0)); // Index 1 of palette is always black

    let mut sprites: Vec<(Vec<usize>, String)> = Vec::new();

    for image in images {
        let name = image
            .to_str()
            .unwrap()
            .split('/')
            .last()
            .unwrap()
            .split('.')
            .next()
            .unwrap()
            .to_string()
            .to_uppercase();

        let img = Reader::open(image)?.decode()?;
        let mut sprite: Vec<usize> = Vec::new();

        for pixel in img.pixels() {
            let [r, g, b, a] = pixel.2 .0;

            // If the color doesn't exist on the palette, add it to the palette and push the latest
            // index to the sprite
            if !palette.contains(&(r, g, b)) {
                palette.push((r, g, b));
                sprite.push(palette.len() - 1);

            // If the opacity is 0 then it's a transparent pixel, so push index 0, which corresponds to
            // a transparent pixel
            } else if a == 0 {
                sprite.push(0);

            // If the color is black, push index 1, which corresponds to black.
            } else if (r, g, b) == (0, 0, 0) {
                sprite.push(1);

            // If the color already exists and it's not transparent or black, find the index and push
            // it to the sprite
            } else {
                let index = palette.iter().position(|c| c == &(r, g, b)).unwrap_or(0);
                sprite.push(index);
            }
        }

        sprites.push((sprite, name));
    }

    let mut file = File::create("sprites.rs")?;
    file.write_all(collect(&palette, &sprites).as_bytes())?;
    println!("Done!");

    Ok(())
}

#[inline]
fn get_image_paths() -> Result<impl Iterator<Item = PathBuf>, String> {
    let mut args = std::env::args();
    let _ = args.next();
    let folder_path = match args.next() {
        Some(arg) => {
            let folder_path = PathBuf::from(arg);
            if folder_path.is_dir() {
                folder_path
            } else {
                return Err(String::from("Path is not a folder"));
            }
        }
        None => {
            return Err(String::from("No path argument found"));
        }
    };

    Ok(WalkDir::new(folder_path)
        .into_iter()
        .filter_map(Result::ok) // Only get Ok entries
        .filter(|i| {
            // Only get FILES of which the metadata is Ok
            if i.metadata().is_ok() {
                i.metadata().unwrap().is_file()
            } else {
                false
            }
        })
        .map(|i| i.path().to_path_buf())) // Get the file paths and convert them to PathBuf
}

#[inline]
fn collect(palette: &[(u8, u8, u8)], sprites: &[(Vec<usize>, String)]) -> String {
    let mut collected: String = String::from("const PALETTE: [(u8, u8, u8); ");
    collected.push_str(&format!("{}] = [", palette.len()));

    for (r, g, b) in palette {
        // 8 bit to 6 bit color conversion
        // embedded_graphics Rgb666 color truncates the 2 most significant bits when creating a new
        // color from (u8, u8, u8), so the 8 to 6 bit conversion has to be done previously for
        // correct color rendering.
        let r = ((*r as f32 / 256.0) * 64.0) as u8;
        let g = ((*g as f32 / 256.0) * 64.0) as u8;
        let b = ((*b as f32 / 256.0) * 64.0) as u8;

        collected.push_str(&format!("({r}, {g}, {b}),"));
    }

    collected.push_str("];\n");

    for (sprite, name) in sprites {
        collected.push_str(&format!("const {name}: [usize; {}] = [", sprite.len()));
        for color_code in sprite {
            collected.push_str(&format!("{color_code}, "));
        }
        collected.push_str("];\n");
    }

    collected
}
