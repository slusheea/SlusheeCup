# SlusheeCup v1
A retro handheld console powered by embedded rust

## Design goals
### Firmware
 - [ ] 255 colors arbitrary color palette
 - [ ] Unlimited* abitrarily sized sprites
 - [ ] Unlimited* composition layers
 - [ ] Multithreaded hardware handling
 
### Hardware
 - [ ] Gameboy Advanced inspired silouhette
 - [ ] HackClub Sprig inspired left and right D-pads
 - [ ] Powered by 2 18650 batteries
 - [ ] Removeable Raspberry Pi Pico
