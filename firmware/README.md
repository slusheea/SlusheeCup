# Flashing the firmware
### For debug builds (debug probe needed)
Build debug firmware:
```bash
just build
```
Flash debug firmware:
```bash
just flash
```
Build and flash debug firmware:
```bash
just build flash
```

### For release builds
Build and flash release firmware through debug probe:
```bash
just release
```
Build and flash release firmware through USB port:
```bash
just usb
```

# Dependencies
 - [just](https://github.com/casey/just) command runner, installed with Cargo
 - [flip-link](https://crates.io/crates/flip-link) stack overflow protection for embedded, installed with Cargo
 - `thumbv6m-none-eabi` compile target, added with Rustup

### For probe flashing 
- [probe-rs-cli](https://crates.io/crates/probe-rs-cli) debug probe manager, installed with Cargo

### For usb flashing 
- [elf2uf2-rs](https://crates.io/crates/elf2uf2-rs) elf to uf2 firmware binary converter, installed with Cargo with the `--locked` flag. If installing fails, you might be missing the `libudev-devel` package.

Need help setting up to flash embedded rust on the pico? I have a guide: https://slushee.dev/pico-tutorial/getting-started/
