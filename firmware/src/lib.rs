#![no_std]
use core::{convert::Infallible, option::Option::Some};

use cortex_m::delay::Delay;
use rp_pico::{
    hal::gpio::{pin::bank0::*, Input, Output, Pin, PinState, PullDown, PushPull},
    Pins,
};
use embedded_hal::digital::v2::InputPin;

use display_interface_parallel_gpio::{Generic8BitBus, PGPIO8BitInterface};
use mipidsi::{error::InitError, Builder};

use embedded_graphics::{
    pixelcolor::Rgb666,
    prelude::*,
    primitives::{Circle, PrimitiveStyleBuilder},
};

// Screen dimensions
pub const HEIGHT: i32 = 240;
pub const WIDTH: i32 = 320;

///
/// Used to refer to the display.
///
/// All the pins are pre-defined since the pads on the PCB aren't supposed to change.
pub type Display = mipidsi::Display<
    PGPIO8BitInterface<
        Generic8BitBus<
            Pin<Gpio15, Output<PushPull>>,
            Pin<Gpio14, Output<PushPull>>,
            Pin<Gpio13, Output<PushPull>>,
            Pin<Gpio12, Output<PushPull>>,
            Pin<Gpio11, Output<PushPull>>,
            Pin<Gpio10, Output<PushPull>>,
            Pin<Gpio9, Output<PushPull>>,
            Pin<Gpio8, Output<PushPull>>,
        >,
        Pin<Gpio6, Output<PushPull>>,
        Pin<Gpio5, Output<PushPull>>,
    >,
    mipidsi::models::ILI9341Rgb666,
    Pin<Gpio7, Output<PushPull>>,
>;

///
/// Holds all the peripherals of the console.
pub struct Console {
    pub display: Display,
    a_button: Pin<Gpio21, Input<PullDown>>,
    b_button: Pin<Gpio20, Input<PullDown>>,
    dpad_u: Pin<Gpio16, Input<PullDown>>,
    dpad_l: Pin<Gpio17, Input<PullDown>>,
    dpad_r: Pin<Gpio18, Input<PullDown>>,
    dpad_d: Pin<Gpio19, Input<PullDown>>,
    // uart0 tx = Gpio0
    // uart0 rx = Gpio1
    // buzzer = Gpio28
    // audio mute switch = Gpio27
}

///
/// Represents the state of the inputs the console has.
/// `true` means the button is being pressed.
pub struct Inputs {
    /// A button
    pub a: bool,
    /// B button
    pub b: bool,
    /// Dpad Up
    pub u: bool,
    /// Dpad Left
    pub l: bool,
    /// Dpad Right
    pub r: bool,
    /// Dpad Down
    pub d: bool,
}

///
/// Errors that `draw_sprite` can return.
///  - `InconsistentSize` will be returned if the calculated number of pixels from the size is not the
/// same as the number of pixels on the sprite
///  - `TooManyColors` will be returned if the palette has more than 256 colors. This is done
///    because the sprite color references are `u8`s. They aren't `usize`s to save space
///  - `OutOfBounds` will be returned if drawing the sprite is attempted where it won't be
///    rendered
///  - `DisplayError` will be returned if any pixel can't be written to the screen
#[derive(Debug)]
pub enum DrawError {
    InconsistentSize,
    OutOfBounds,
    DisplayError,
    TooManyColors,
}

impl Console {
    ///
    /// Initializes all the console peripherals
    ///
    /// # Arguments
    /// `pins`: Pins struct from the bsp
    /// `delay`: Delay struct from `cortex_m`
    ///
    /// # Errors
    /// Will error if the parallel bus or the display driver fail to initialize.
    pub fn new(pins: Pins, delay: &mut Delay) -> Result<Self, InitError<Infallible>> {
        let a_button = pins.gpio21.into_pull_down_input();
        let b_button = pins.gpio20.into_pull_down_input();
        let dpad_u = pins.gpio16.into_pull_down_input();
        let dpad_l = pins.gpio17.into_pull_down_input();
        let dpad_r = pins.gpio18.into_pull_down_input();
        let dpad_d = pins.gpio19.into_pull_down_input();

        let p0 = pins.gpio15.into_push_pull_output();
        let p1 = pins.gpio14.into_push_pull_output();
        let p2 = pins.gpio13.into_push_pull_output();
        let p3 = pins.gpio12.into_push_pull_output();
        let p4 = pins.gpio11.into_push_pull_output();
        let p5 = pins.gpio10.into_push_pull_output();
        let p6 = pins.gpio9.into_push_pull_output();
        let p7 = pins.gpio8.into_push_pull_output();

        let rst = pins.gpio7.into_push_pull_output_in_state(PinState::High);
        let dc = pins.gpio6.into_push_pull_output();
        let wr = pins.gpio5.into_push_pull_output_in_state(PinState::High);
        // RD pin must be pulled high at all times for the display to work.

        let bus = Generic8BitBus::new((p0, p1, p2, p3, p4, p5, p6, p7))?;
        let di = PGPIO8BitInterface::new(bus, dc, wr);

        let display = Builder::ili9341_rgb666(di)
            .with_color_order(mipidsi::ColorOrder::Bgr)
            .with_orientation(mipidsi::Orientation::LandscapeInverted(false))
            .init(delay, Some(rst))?;

        Ok(Self {
            display,
            a_button,
            b_button,
            dpad_u,
            dpad_l,
            dpad_r,
            dpad_d,
        })
    }
    
    /// TODO: Swap for interrupts
    /// Returns the state of the console's inputs. 
    pub fn read_inputs(&mut self) -> Inputs {
        Inputs {
            a: self.a_button.is_high().unwrap(),
            b: self.b_button.is_high().unwrap(),
            u: self.dpad_u.is_high().unwrap(),
            l: self.dpad_l.is_high().unwrap(),
            r: self.dpad_r.is_high().unwrap(),
            d: self.dpad_d.is_high().unwrap(),
        }
    }

    ///
    /// Draws a sprite to the display
    ///
    /// # Arguments
    /// - `palette` Palette of colors provided by `sprite_converter`
    /// - `sprite` One of the sprites provided by `sprite_converter` to be drawn
    /// - `(x_size, y_size)` Horizontal and verical size of the sprite in pixels
    /// - `(x_pos, y_pos)` X and Y position to draw the sprite on the screen, respect it's top left
    ///   corner. (0, 0) on the screen is also the top left corner.
    ///
    /// # Errors
    ///  - `InconsistentSize` will be returned if the calculated number of pixels from the size is not the
    /// same as the number of pixels on the sprite
    ///  - `TooManyColors` will be returned if the palette has more than 256 colors. This is done
    ///    because the sprite color references are `u8`s. They aren't `usize`s to save space
    ///  - `OutOfBounds` will be returned if drawing the sprite is attempted where it won't be
    ///    rendered
    ///  - `DisplayError` will be returned if any pixel can't be written to the screen
    pub fn draw_sprite(
        &mut self,
        palette: &[(u8, u8, u8)],
        sprite: &[u8],
        (x_size, y_size): (usize, usize),
        (x_pos, y_pos): (i16, i16),
    ) -> Result<(), DrawError> {
        const TRANSPARENT: u8 = 0;

        if x_size * y_size != sprite.len() {
            return Err(DrawError::InconsistentSize);
        }

        if palette.len() > 256 {
            return Err(DrawError::TooManyColors);
        }

        // TODO: Implement more extensive checking: check if the sprite will also not be rendere
        // to the left of the screen
        if x_pos > 320 || y_pos > 240 {
            return Err(DrawError::OutOfBounds);
        }

        for x in 0..x_size {
            for y in 0..y_size {
                let color_code = sprite[y * x_size + x];
                let x = x as i16 + x_pos;
                let y = y as i16 + y_pos;

                if color_code == TRANSPARENT || x < 0 || y < 0 {
                    continue;
                }

                let (r, g, b) = palette[color_code as usize];
                if self
                    .display
                    .set_pixel(x as u16, y as u16, Rgb666::new(r, g, b))
                    .is_err()
                {
                    return Err(DrawError::DisplayError);
                }
            }
        }
        Ok(())
    }
    
    ///
    /// Draws a representation of the console's inputs on the screen
    ///
    /// # Arguments
    ///  - `state`: an Inputs struct determining which inputs should be drawn active or inactive
    pub fn draw_inputs(&mut self, state: Inputs) {
        let empty = PrimitiveStyleBuilder::new()
            .stroke_color(Rgb666::WHITE)
            .stroke_width(3)
            .build();

        let full = PrimitiveStyleBuilder::new()
            .stroke_color(Rgb666::WHITE)
            .stroke_width(3)
            .fill_color(Rgb666::WHITE)
            .build();
        
        // TODO: Write the representation of the inputs
    }
}
