#![no_std]
#![no_main]

use bsp::entry;
use bsp::hal::{
    clocks::{init_clocks_and_plls, Clock},
    pac,
    rosc::RingOscillator,
    sio::Sio,
    watchdog::Watchdog,
};
use embedded_graphics::{
    mono_font::{ascii::FONT_10X20, MonoTextStyle},
    pixelcolor::Rgb666,
    prelude::*,
};
use rand::seq::SliceRandom;
use rp_pico as bsp;

use defmt_rtt as _;
use panic_probe as _;

// Current eg_seven_segment crate uses an outdated embedded-graphics dependency which breaks with
// newer ones. You must clone the eg_seven_segment crate outside the SlusheeCup directory and
// change the embedded_graphics version to the latests for this to work.
use eg_seven_segment::{Digit, SevenSegmentStyleBuilder};

use slushee_cup::{Console, WIDTH, HEIGHT};

#[entry]
fn main() -> ! {
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);
    let mut rosc = RingOscillator::new(pac.ROSC).initialize();

    // External high-speed crystal on the pico board is 12Mhz
    let external_xtal_freq_hz = 12_000_000u32;
    let clocks = init_clocks_and_plls(
        external_xtal_freq_hz,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let mut console = Console::new(pins, &mut delay).unwrap();

    console.display.clear(Rgb666::BLACK).unwrap();

    let text_style = MonoTextStyle::new(&FONT_10X20, Rgb666::WHITE);
    let segment_style = SevenSegmentStyleBuilder::new()
        .digit_size(Size::new(50, 100)) // digits are 10x20 pixels
        .digit_spacing(5)
        .segment_width(10)
        .segment_color(Rgb666::WHITE)
        .build();

    const DIGIT_X_CENTER: i32 = WIDTH / 2 - 25;
    const DIGIT_Y_CENTER: i32 = HEIGHT / 2 - 50;

    let mut solutions = SOLUTIONS;
    solutions.shuffle(&mut rosc);

    console.display.clear(Rgb666::BLACK).unwrap();

    loop {
        for (digit, solution) in solutions {
            let digit: char = digit;
            let solution: [bool; 4] = solution;

            console.display.clear(Rgb666::BLACK).unwrap();

            Digit::new(
                digit.try_into().unwrap(),
                Point::new(DIGIT_X_CENTER, DIGIT_Y_CENTER),
            )
            .into_styled(segment_style)
            .draw(&mut console.display)
            .unwrap();

            todo!("Read and verify input");
        }
    }
}

const SOLUTIONS: [(char, [bool; 4]); 16] = [
    ('0', [false, false, false, false]),
    ('1', [false, false, false, true]),
    ('2', [false, false, true, false]),
    ('3', [false, false, true, true]),
    ('4', [false, true, false, false]),
    ('5', [false, true, false, true]),
    ('6', [false, true, true, false]),
    ('7', [false, true, true, true]),
    ('8', [true, false, false, false]),
    ('9', [true, false, false, true]),
    ('a', [true, false, true, false]),
    ('b', [true, false, true, true]),
    ('c', [true, true, false, false]),
    ('d', [true, true, false, true]),
    ('e', [true, true, true, false]),
    ('f', [true, true, true, true]),
];

